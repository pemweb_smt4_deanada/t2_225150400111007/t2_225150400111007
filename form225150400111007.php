<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Input Data</title>
</head>
<body>
    <h1>Form Input Data</h1>
    <form method="POST" action="process.php">
        <!-- Input Teks -->
        <label for="nama">Nama:</label>
        <input type="text" id="nama" name="nama"><br>

        <!-- Radio Button -->
        <label>Jenis Kelamin:</label>
        <input type="radio" id="laki" name="jenis_kelamin" value="Laki-laki">
        <label for="laki">Laki-laki</label>
        <input type="radio" id="perempuan" name="jenis_kelamin" value="Perempuan">
        <label for="perempuan">Perempuan</label><br>

        <!-- Combo Box -->
        <label for="agama">Agama:</label>
        <select id="agama" name="agama">
            <option value="Islam">Islam</option>
            <option value="Protestan">Protestan</option>
            <option value="Katolik">Katolik</option>
            <option value="Hindu">Hindu</option>
            <option value="Buddha">Buddha</option>
            <option value="Konghucu">Konghucu</option>
        </select><br>

        <input type="submit" value="Submit">
    </form>
</body>
</html>
